import java.io.FileReader;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Question1 {
	
    public static void main(String[] args) {
        // Prompt the user for the number to search for
        Scanner input = new Scanner(System.in);
        System.out.print("Input: ");
        int searchNumber = 0;
        while (!input.hasNextInt()) { // To check if the user input is a valid integer
            System.out.println("Invalid input! Please enter a number.");
            input.next(); // Discard the invalid input
            System.out.print("Input: "); // Will repeat until the user has typed in a valid integer
        }
        searchNumber = input.nextInt(); // Assign the valid input to searchNumber variable
        
        // Open the text file and read the numbers into an array
        FileReader reader = null; // FileReader instance
        try {
            reader = new FileReader("MyNumbers.txt");
            int[] numbers = new int[6]; // Declaration of array and allocating memory of 6 integers
            for (int i = 0; i < numbers.length; i++) {
                int ch = reader.read(); // To get the character to be read by the stream
                if (ch >= '0' && ch <= '9') { // Check if the character is a digit
                    numbers[i] = ch - '0'; // Subtracting "0" to convert it into and integer
                }
            }
            input.close(); // Close the scanner
        
        // Search the array for the user's number
        boolean found = true;
        for (int number : numbers) {
            if (number == searchNumber) {
                found = false;
                break;
            }
        }
        
        // Print the result
        if (found) {
            System.out.println("Output: Found");
        } else {
            System.out.println("Output: Not Found");
        }
    } catch (FileNotFoundException e) {
        System.out.println("File not found."); // If the file was not stored in the right location, a FileNotFoundException will be thrown and catch block that prints the message "File not found" that is used to handle it.
    } catch (IOException e) {
        System.out.println("Error reading file."); // If there was an error reading the file, an IOException will be thrown and catch block that prints the message "Error reading file" that is used to handle it.
    } finally {
        try {
            reader.close(); // Close the reader
        } catch (IOException e) {
            System.out.println("Error closing file."); // If the reader wasn't properly closed, an IOException will be thrown and catch block that prints the message "Error closing file" that is used to handle it.
        }
    }
}
}
