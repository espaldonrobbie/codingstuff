import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Question2 {
	
    public static void main(String[] args) {
        // File name where the original and modified text will be stored
        String fileName = "RandomNames.txt";
        String searchString;
        String replacementString;
        String text = "";
        try {
            // Read the original text from the file
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            String line = reader.readLine();
            while (line != null) { // Loop through all the lines in the file
                text += line + "\n"; // Add new line character at the end of each line
                line = reader.readLine();
            }
            reader.close(); // Close the reader
            Scanner input = new Scanner(System.in); 
            System.out.print("Input: "); // Input the string to replace
            searchString = input.nextLine(); 
            // Check if the text contains the search string
            if (text.contains(searchString)) {
                System.out.print("New word: "); // Input the new string 
                replacementString = input.nextLine();
                text = text.replace(searchString, replacementString); // To replace an instance of a particular string with a replacement string
                // Write the modified text to the file
                BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
                writer.write(text);
                System.out.println("Output: \n" + text); // Modified text
                writer.close();
                input.close();
            } else {
                System.out.println("<Invalid text>"); // If string is not on the text file, it will print invalid text.
            }
        } catch (IOException e) {
            System.out.println("An error occurred."); // If file is corrupted or used by another program, print error.
            e.printStackTrace();
        }
    }
}
